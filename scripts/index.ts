import { resolve } from "node:path";
import {
  scaleBand,
  scaleDiverging,
  scaleLinear,
  scaleLog,
  scaleOrdinal,
  scalePoint,
  scalePow,
  scaleQuantile,
  scaleQuantize,
  scaleSequential,
  scaleSymlog,
  scaleThreshold,
  scaleTime,
} from "d3-scale";
import type {
  ScaleBand,
  ScaleDiverging,
  ScaleLinear,
  ScaleLogarithmic,
  ScaleOrdinal,
  ScalePoint,
  ScalePower,
  ScaleQuantile,
  ScaleQuantize,
  ScaleSequential,
  ScaleSymLog,
  ScaleThreshold,
  ScaleTime,
} from "d3-scale";
import { outputJsonSync } from "fs-extra";

interface Conditions {
  scaleHasOwn: string;
  scaleHasOwnProperty: string;
}

type D3Scale =
  // biome-ignore lint/suspicious/noExplicitAny: to have a type that simulates the union of the default D3 scale types
  | ScaleBand<any>
  // biome-ignore lint/suspicious/noExplicitAny: to have a type that simulates the union of the default D3 scale types
  | ScaleDiverging<any>
  // biome-ignore lint/suspicious/noExplicitAny: to have a type that simulates the union of the default D3 scale types
  | ScaleLinear<any, any>
  // biome-ignore lint/suspicious/noExplicitAny: to have a type that simulates the union of the default D3 scale types
  | ScaleLogarithmic<any, any>
  // biome-ignore lint/suspicious/noExplicitAny: to have a type that simulates the union of the default D3 scale types
  | ScaleOrdinal<any, any>
  // biome-ignore lint/suspicious/noExplicitAny: to have a type that simulates the union of the default D3 scale types
  | ScalePoint<any>
  // biome-ignore lint/suspicious/noExplicitAny: to have a type that simulates the union of the default D3 scale types
  | ScalePower<any, any>
  // biome-ignore lint/suspicious/noExplicitAny: to have a type that simulates the union of the default D3 scale types
  | ScaleQuantile<any>
  // biome-ignore lint/suspicious/noExplicitAny: to have a type that simulates the union of the default D3 scale types
  | ScaleQuantize<any>
  // biome-ignore lint/suspicious/noExplicitAny: to have a type that simulates the union of the default D3 scale types
  | ScaleSequential<any>
  // biome-ignore lint/suspicious/noExplicitAny: to have a type that simulates the union of the default D3 scale types
  | ScaleSymLog<any, any>
  // biome-ignore lint/suspicious/noExplicitAny: to have a type that simulates the union of the default D3 scale types
  | ScaleThreshold<any, any>
  // biome-ignore lint/suspicious/noExplicitAny: to have a type that simulates the union of the default D3 scale types
  | ScaleTime<any, any>;

function isUndefined(value: unknown): value is undefined {
  return typeof value === "undefined";
}

function isAllDistinct(arr: unknown[]): boolean {
  return arr.length === new Set(arr).size;
}

function removeUnique<T>(arr: T[]): T[] {
  return [...new Set(arr)].filter((i) => arr.indexOf(i) !== arr.lastIndexOf(i));
}

function getUniqueProps(scales: D3Scale[]): string[] {
  const allProps = scales.flatMap((scale) => Object.keys(scale));

  const uniqueProps = allProps.filter(
    (prop) => allProps.indexOf(prop) === allProps.lastIndexOf(prop),
  );

  return uniqueProps;
}

function generateConditions(scale: D3Scale, uniqueProps?: string[]): Conditions {
  const sep = " && ";
  const varName = "value";

  const scaleProps = Object.keys(scale);
  // console.log(scaleProps, scaleProps.length);

  const scaleHasOwn = scaleProps
    .flatMap((prop) => {
      if (isUndefined(uniqueProps) || uniqueProps.includes(prop)) {
        return [`Object.hasOwn(${varName}, '${prop}')`];
      }

      return [];
    })
    .join(sep);

  const scaleHasOwnProperty = scaleProps
    .flatMap((prop) => {
      if (isUndefined(uniqueProps) || uniqueProps.includes(prop)) {
        return [`Object.prototype.hasOwnProperty.call(${varName}, '${prop}')`];
      }

      return [];
    })
    .join(sep);

  return { scaleHasOwn, scaleHasOwnProperty };
}

function main() {
  // https://d3js.org/d3-scale/band
  const bandScale = scaleBand();

  // https://d3js.org/d3-scale/diverging
  const divergingScale = scaleDiverging();

  // https://d3js.org/d3-scale/linear
  const linearScale = scaleLinear();

  // https://d3js.org/d3-scale/log
  const logScale = scaleLog();

  // https://d3js.org/d3-scale/ordinal
  const ordinalScale = scaleOrdinal();

  // https://d3js.org/d3-scale/point
  const pointScale = scalePoint();

  // https://d3js.org/d3-scale/pow
  const powScale = scalePow();

  // https://d3js.org/d3-scale/quantile
  const quantileScale = scaleQuantile();

  // https://d3js.org/d3-scale/quantize
  const quantizeScale = scaleQuantize();

  // https://d3js.org/d3-scale/sequential
  const sequentialScale = scaleSequential();

  // https://d3js.org/d3-scale/symlog
  const symlogScale = scaleSymlog();

  // https://d3js.org/d3-scale/threshold
  const thresholdScale = scaleThreshold();

  // https://d3js.org/d3-scale/time
  const timeScale = scaleTime();

  const uniqueProps = getUniqueProps([
    bandScale,
    divergingScale,
    linearScale,
    logScale,
    ordinalScale,
    pointScale,
    powScale,
    quantileScale,
    quantizeScale,
    sequentialScale,
    symlogScale,
    thresholdScale,
    timeScale,
  ]);

  const uniqueOutput = {
    scaleBand: generateConditions(bandScale, uniqueProps),
    scaleDiverging: generateConditions(divergingScale, uniqueProps),
    scaleLinear: generateConditions(linearScale, uniqueProps),
    scaleLog: generateConditions(logScale, uniqueProps),
    scaleOrdinal: generateConditions(ordinalScale, uniqueProps),
    scalePoint: generateConditions(pointScale, uniqueProps),
    scalePow: generateConditions(powScale, uniqueProps),
    scaleQuantile: generateConditions(quantileScale, uniqueProps),
    scaleQuantize: generateConditions(quantizeScale, uniqueProps),
    scaleSequential: generateConditions(sequentialScale, uniqueProps),
    scaleSymlog: generateConditions(symlogScale, uniqueProps),
    scaleThreshold: generateConditions(thresholdScale, uniqueProps),
    scaleTime: generateConditions(timeScale, uniqueProps),
  };

  const allOutput = {
    scaleBand: generateConditions(bandScale),
    scaleDiverging: generateConditions(divergingScale),
    scaleLinear: generateConditions(linearScale),
    scaleLog: generateConditions(logScale),
    scaleOrdinal: generateConditions(ordinalScale),
    scalePoint: generateConditions(pointScale),
    scalePow: generateConditions(powScale),
    scaleQuantile: generateConditions(quantileScale),
    scaleQuantize: generateConditions(quantizeScale),
    scaleSequential: generateConditions(sequentialScale),
    scaleSymlog: generateConditions(symlogScale),
    scaleThreshold: generateConditions(thresholdScale),
    scaleTime: generateConditions(timeScale),
  };

  const output = {
    unique: uniqueOutput,
    all: allOutput,
  };

  const allOutputConditions: string[] = Object.values(allOutput).flatMap((singleOutput) =>
    Object.values(singleOutput),
  );

  // biome-ignore lint/suspicious/noConsoleLog: to check for duplicate conditions
  console.log(isAllDistinct(allOutputConditions), removeUnique(allOutputConditions));

  outputJsonSync(resolve(__dirname, "scales.json"), output, { spaces: 2 });
}

main();
