# Notes

- https://github.com/sniptt-official/guards:
  - https://github.com/sniptt-official/guards/blob/main/lib/guards/primitives.ts
  - https://github.com/sniptt-official/guards/blob/main/test/guards/primitives.test.ts
- https://github.com/mscharley/generic-type-guard
- https://github.com/gabrielricardourbina/type-guard:
  - https://github.com/gabrielricardourbina/type-guard/blob/main/src/is-string.ts
- https://github.com/michaeljscript/create-typeguard
- https://mainawycliffe.dev/blog/custom-type-guards-in-typescript/
- https://zanza00.gitbook.io/learn-fp-ts/option/type-guards
- https://github.com/unjs/template:
  - https://github.com/unjs/template/blob/main/.editorconfig
  - https://github.com/unjs/unbuild
  - https://github.com/unjs/template/blob/main/package.json
- https://www.typescriptlang.org/docs/handbook/2/narrowing.html#using-type-predicates
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
- https://d3js.org/d3-scale
- https://www.npmjs.com/package/d3-scale
- https://www.npmjs.com/package/@types/d3-scale
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/hasOwn: `Object.hasOwn(foo, "bar");`
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/hasOwnProperty#using_hasownproperty_as_a_property_name: `Object.prototype.hasOwnProperty.call(foo, "bar");`
- https://www.typescriptlang.org/docs/handbook/2/typeof-types.html#limitations
- https://typescript.tv/new-features/enhancing-return-types-with-function-overloading-in-typescript/
- https://github.com/sindresorhus/type-fest/tree/main?tab=readme-ov-file#type-guard
- https://byby.dev/ts-function-overloading
- https://www.stefanjudis.com/snippets/filter-and-map-array-values-with-a-flatmap-one-liner/
- https://github.com/thinkmill/emery:
  - https://github.com/Thinkmill/emery/blob/ef3ee1eb539af6c4f635512282ff7bd310028c7e/src/guards.ts#L27
  - https://emery-ts.vercel.app/docs/guards#is-undefined
- https://ui.dev/check-for-undefined-javascript
- https://www.30secondsofcode.org/js/s/unique-values-in-array-remove-duplicates/
- https://github.com/d3/d3-scale/blob/main/src/diverging.js
- https://github.com/d3/d3-scale/blob/main/src/sequential.js

## Commands

```bash
npm install -D @biomejs/biome sort-package-json jiti npm-run-all2 d3-scale @types/d3-scale fs-extra @types/fs-extra
```
